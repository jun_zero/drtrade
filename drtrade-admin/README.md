# drtrade
基于SpringBoot框架的权限管理系统，支持操作权限和数据权限，后端采用SpringBoot、Mybatis、Shiro，前端采用adminLTE、vue.js、bootstrap-table、tree-grid、layer，对前后端进行封装，可快速完成CRUD的开发，基于项目结构通过代码生成器可生成前端后台部分代码，更加方便地进行二次开发。项目采用Maven分模块构建，方便扩展自定义模块。

### 传送门

- 项目文档：[http://dp-dev.mydoc.io/](http://dp-dev.mydoc.io/)

### 应用分层（参考阿里巴巴Java开发手册）

