/**
 * 新增-代理商js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		drtradeAgency: {
			parentId: 0,
			parentName: '总代理'
		}
	},
	methods : {
        agencyTree: function(){
            dialogOpen({
                id: 'layerAgencyTree',
                title: '选择上级代理商',
                url: 'drtrade_admin/agency/tree.html?_' + $.now(),
                scroll : true,
                width: "300px",
                height: "450px",
                yes : function(iframeId) {
                    top.frames[iframeId].vm.acceptClick();
                }
            })
        },
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: '../../drtrade/agency/save?_' + $.now(),
		    	param: vm.drtradeAgency,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
