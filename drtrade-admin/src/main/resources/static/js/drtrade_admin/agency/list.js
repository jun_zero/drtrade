/**
 * 代理商js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
	$(window).resize(function() {
        TreeGrid.table.resetHeight({height: $(window).height()-100});
	});
}

function getGrid() {
    var colunms = TreeGrid.initColumn();
    var table = new TreeTable(TreeGrid.id, '../../drtrade/agency/list?_' + $.now(), colunms);
    table.setExpandColumn(2);
    table.setIdField("agencyId");
    table.setCodeField("agencyId");
    table.setParentCodeField("parentId");
    table.setExpandAll(false);
    table.setHeight($(window).height()-100);
    table.init();
    TreeGrid.table = table;
	/*$('#dataGrid').bootstrapTableEx({
		url: '../../drtrade/agency/list?_' + $.now(),
		height: $(window).height()-56,
		queryParams: function(params){
			params.name = vm.keyword;
			return params;
		},
		columns: [
			{checkbox: true},
			{field : "agencyId", title : "代理商id", width : "100px"}, 
			{field : "agencyName", title : "代理商名称", width : "100px"}, 
			{field : "linkman", title : "联系人", width : "100px"}, 
			{field : "linktel", title : "联系电话", width : "100px"}, 
			{field : "address", title : "地址", width : "300px"},
			{field : "parentId", title : "上级代理商", width : "100px"}
		]
	})*/
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: ''
	},
	methods : {
		load: function() {
            TreeGrid.table.refresh({query:{"agencyName": vm.keyword}});
		},
		save: function() {
			dialogOpen({
				title: '新增代理商',
				url: 'drtrade_admin/agency/add.html?_' + $.now(),
				width: '420px',
				height: '350px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
            var ck = TreeGrid.table.getSelectedRow();
			if(checkedRow(ck)){
				dialogOpen({
					title: '编辑代理商',
					url: 'drtrade_admin/agency/edit.html?_' + $.now(),
					width: '420px',
					height: '350px',
					success: function(iframeId){
						top.frames[iframeId].vm.drtradeAgency.agencyId = ck[0].id;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
            var ck = TreeGrid.table.getSelectedRow(), ids = [];
			if(checkedArray(ck)){
				$.each(ck, function(idx, item){
					ids[idx] = item.id;
				});
				$.RemoveForm({
					url: '../../drtrade/agency/remove?_' + $.now(),
			    	param: ids,
			    	success: function(data) {
			    		vm.load();
			    	}
				});
			}
		},
        genQrcode: function() {
            var ck = TreeGrid.table.getSelectedRow(), ids = [];
            if(checkedRow(ck)){
                $.ajax({
                    url : '../../drtrade/agency/genQrcode?_' + $.now(),
                    data : JSON.stringify({agencyId: ck[0].id}),
                    type : 'post',
                    dataType : 'json',
                    contentType : 'application/json',
                    success : function(data) {
                        if (data.code == '500') {
                            dialogAlert(data.msg, 'error');
                        } else if (data.code == '0') {
                            dialogMsg(data.msg, 'success');
                            vm.load();
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        dialogLoading(false);
                        if(XMLHttpRequest.responseJSON.code == 401){
                            toUrl('login.html');
                        } else if(textStatus=="error"){
                            dialogMsg("请求超时，请稍候重试...", "error");
                        } else {
                            dialogMsg(errorThrown, 'error');
                        }
                    },
                    beforeSend : function() {
                        dialogLoading(true);
                    },
                    complete : function() {
                        dialogLoading(false);
                    }
                });
            }
        }
	}
})

var TreeGrid = {
    id: "dataGrid",
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
TreeGrid.initColumn = function () {
    var columns = [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'agencyId', visible: false, align: 'center', valign: 'middle', width: '30px'},
        {title: '代理商名称', field: 'agencyName', align: 'center', valign: 'middle', width: '100px'},
        {title: '联系人', field: 'linkman', align: 'center', valign: 'middle', width: '100px'},
        {title: '联系电话', field: 'linktel', align: 'center', valign: 'middle', width: '100px'},
        {title: '联系地址', field: 'address', align: 'center', valign: 'middle', width: '100px'},
        {title: '上级代理商', field: 'parentId', align: 'center', valign: 'middle', width: '100px', formatter: function(item, index){
            if(item.parentId === 0){
                return '总代理';
            }
            if(item.parentId === 1){
                return item.parentName;
            }
        }},
        {title: '二维码',field: 'qrcodeList',align: 'middle', width: '150px', formatter:function(item){
            if(item.qrcodeList[0].qrcodeName){
                var aHtml = "";
                for(var i=0;i<item.qrcodeList.length;i++){
                    aHtml+= '<a href="../../'+item.qrcodeList[i].qrcodePath+'" target="_blank">'+item.qrcodeList[i].qrcodeName+'</>   '
                }
                return aHtml;
            }else {
                return '未生成二维码'
            }
        }}
    ]
    return columns;
};