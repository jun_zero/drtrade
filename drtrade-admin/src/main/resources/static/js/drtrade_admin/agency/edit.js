/**
 * 编辑-代理商js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		drtradeAgency: {
		}
	},
	methods : {
        agencyTree: function(){
            dialogOpen({
                id: 'layerAgencyTree',
                title: '选择上级代理商',
                url: 'drtrade_admin/agency/tree.html?_' + $.now(),
                scroll : true,
                width: "300px",
                height: "450px",
                yes : function(iframeId) {
                    top.frames[iframeId].vm.acceptClick();
                }
            })
        },
		setForm: function() {
			$.SetForm({
				url: '../../drtrade/agency/info?_' + $.now(),
		    	param: vm.drtradeAgency.agencyId,
		    	success: function(data) {
		    		vm.drtradeAgency = data;
		    	}
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.ConfirmForm({
		    	url: '../../drtrade/agency/update?_' + $.now(),
		    	param: vm.drtradeAgency,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})