package net.chenlin.dp.modules.drtrade.manager;

import java.util.List;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.modules.drtrade.entity.DrtradeMemberEntity;

/**
 * 
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月27日 下午4:45:46
 */
public interface DrtradeMemberManager {

	List<DrtradeMemberEntity> listDrtradeMember(Page<DrtradeMemberEntity> page, Query search);
	
	int saveDrtradeMember(DrtradeMemberEntity drtradeMember);
	
	DrtradeMemberEntity getDrtradeMemberById(Long id);
	
	int updateDrtradeMember(DrtradeMemberEntity drtradeMember);
	
	int batchRemove(Long[] id);

	DrtradeMemberEntity getByOpenid(String openid);
}
