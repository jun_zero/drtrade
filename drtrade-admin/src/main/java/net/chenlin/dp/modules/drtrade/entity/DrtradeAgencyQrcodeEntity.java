package net.chenlin.dp.modules.drtrade.entity;

import java.io.Serializable;
import java.util.Date;



/**
 * 代理商二维码
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月31日 上午8:47:17
 */
public class DrtradeAgencyQrcodeEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	private Integer qrcodeId;
	
	/**
	 * 代理商id
	 */
	private Integer agencyId;
	
	/**
	 * 二维码路径
	 */
	private String qrcodePath;
	
	/**
	 * 
	 */
	private String qrcodeName;
	

	public DrtradeAgencyQrcodeEntity() {
		super();
	}

	public void setQrcodeId(Integer qrcodeId) {
		this.qrcodeId = qrcodeId;
	}
	
	public Integer getQrcodeId() {
		return qrcodeId;
	}
	
	public void setAgencyId(Integer agencyId) {
		this.agencyId = agencyId;
	}
	
	public Integer getAgencyId() {
		return agencyId;
	}
	
	public void setQrcodePath(String qrcodePath) {
		this.qrcodePath = qrcodePath;
	}
	
	public String getQrcodePath() {
		return qrcodePath;
	}
	
	public void setQrcodeName(String qrcodeName) {
		this.qrcodeName = qrcodeName;
	}
	
	public String getQrcodeName() {
		return qrcodeName;
	}
	
}
