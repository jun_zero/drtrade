package net.chenlin.dp.modules.quartz.service;

import java.util.Map;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.modules.quartz.entity.QuartzJobEntity;

/**
 * 定时任务
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2017年8月20日 下午11:48:32
 */
public interface QuartzJobService {
	
	Page<QuartzJobEntity> list(Map<String, Object> params);
	
	R saveQuartzJob(QuartzJobEntity job);
	
	R getQuartzJobById(Long jobId);
	
	R updateQuartzJob(QuartzJobEntity job);
	
	R batchRemoveQuartzJob(Long[] id);
	
	R run(Long[] id);
	
	R pause(Long[] id);
	
	R resume(Long[] id);
	
}
