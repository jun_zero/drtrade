package net.chenlin.dp.modules.drtrade.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.modules.drtrade.dao.DrtradeAgencyQrcodeMapper;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyQrcodeEntity;
import net.chenlin.dp.modules.drtrade.manager.DrtradeAgencyQrcodeManager;

/**
 * 代理商二维码
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月31日 上午8:47:17
 */
@Component("drtradeAgencyQrcodeManager")
public class DrtradeAgencyQrcodeManagerImpl implements DrtradeAgencyQrcodeManager {

	@Autowired
	private DrtradeAgencyQrcodeMapper drtradeAgencyQrcodeMapper;
	

	@Override
	public List<DrtradeAgencyQrcodeEntity> listDrtradeAgencyQrcode(Page<DrtradeAgencyQrcodeEntity> page, Query search) {
		return drtradeAgencyQrcodeMapper.listForPage(page, search);
	}

	@Override
	public int saveDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode) {
		return drtradeAgencyQrcodeMapper.save(drtradeAgencyQrcode);
	}

	@Override
	public DrtradeAgencyQrcodeEntity getDrtradeAgencyQrcodeById(Long id) {
		DrtradeAgencyQrcodeEntity drtradeAgencyQrcode = drtradeAgencyQrcodeMapper.getObjectById(id);
		return drtradeAgencyQrcode;
	}

	@Override
	public int updateDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode) {
		return drtradeAgencyQrcodeMapper.update(drtradeAgencyQrcode);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = drtradeAgencyQrcodeMapper.batchRemove(id);
		return count;
	}

	@Override
	public int removeByAgencyId(Integer agencyId){
		int count = drtradeAgencyQrcodeMapper.removeByAgencyId(agencyId);
		return count;
	}
}
