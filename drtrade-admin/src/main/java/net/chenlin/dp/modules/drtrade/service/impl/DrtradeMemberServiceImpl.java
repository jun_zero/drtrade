package net.chenlin.dp.modules.drtrade.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.common.utils.CommonUtils;
import net.chenlin.dp.modules.drtrade.entity.DrtradeMemberEntity;
import net.chenlin.dp.modules.drtrade.manager.DrtradeMemberManager;
import net.chenlin.dp.modules.drtrade.service.DrtradeMemberService;

/**
 * 
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月27日 下午4:45:46
 */
@Service("drtradeMemberService")
public class DrtradeMemberServiceImpl implements DrtradeMemberService {

	@Autowired
	private DrtradeMemberManager drtradeMemberManager;

	@Override
	public Page<DrtradeMemberEntity> listDrtradeMember(Map<String, Object> params) {
		Query query = new Query(params);
		Page<DrtradeMemberEntity> page = new Page<>(query);
		drtradeMemberManager.listDrtradeMember(page, query);
		return page;
	}

	@Override
	public R saveDrtradeMember(DrtradeMemberEntity role) {
		int count = drtradeMemberManager.saveDrtradeMember(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getDrtradeMemberById(Long id) {
		DrtradeMemberEntity drtradeMember = drtradeMemberManager.getDrtradeMemberById(id);
		return CommonUtils.msg(drtradeMember);
	}

	@Override
	public R updateDrtradeMember(DrtradeMemberEntity drtradeMember) {
		int count = drtradeMemberManager.updateDrtradeMember(drtradeMember);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = drtradeMemberManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	@Override
	public DrtradeMemberEntity getByOpenid(String openid) {
		DrtradeMemberEntity drtradeMember = drtradeMemberManager.getByOpenid(openid);
		return drtradeMember;
	}
}
