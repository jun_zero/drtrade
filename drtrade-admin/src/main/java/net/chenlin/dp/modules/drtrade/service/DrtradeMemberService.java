package net.chenlin.dp.modules.drtrade.service;

import java.util.Map;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.modules.drtrade.entity.DrtradeMemberEntity;

/**
 * 
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月27日 下午4:45:46
 */
public interface DrtradeMemberService {

	Page<DrtradeMemberEntity> listDrtradeMember(Map<String, Object> params);
	
	R saveDrtradeMember(DrtradeMemberEntity drtradeMember);
	
	R getDrtradeMemberById(Long id);
	
	R updateDrtradeMember(DrtradeMemberEntity drtradeMember);
	
	R batchRemove(Long[] id);

	DrtradeMemberEntity getByOpenid(String openid);
}
