package net.chenlin.dp.modules.drtrade.dao;

import org.apache.ibatis.annotations.Mapper;

import net.chenlin.dp.modules.drtrade.entity.DrtradeMemberEntity;
import net.chenlin.dp.modules.sys.dao.BaseMapper;

/**
 * 
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月27日 下午4:45:46
 */
@Mapper
public interface DrtradeMemberMapper extends BaseMapper<DrtradeMemberEntity> {
	DrtradeMemberEntity getByOpenid(String openid);
}
