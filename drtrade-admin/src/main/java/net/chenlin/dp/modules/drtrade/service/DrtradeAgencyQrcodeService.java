package net.chenlin.dp.modules.drtrade.service;

import java.util.Map;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyQrcodeEntity;

/**
 * 代理商二维码
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月31日 上午8:47:17
 */
public interface DrtradeAgencyQrcodeService {

	Page<DrtradeAgencyQrcodeEntity> listDrtradeAgencyQrcode(Map<String, Object> params);
	
	R saveDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode);
	
	R getDrtradeAgencyQrcodeById(Long id);
	
	R updateDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode);
	
	R batchRemove(Long[] id);

	R removeByAgencyId(Integer agencyId);
}
