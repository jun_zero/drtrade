package net.chenlin.dp.modules.drtrade.manager.impl;

import java.util.List;

import net.chenlin.dp.common.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.modules.drtrade.dao.DrtradeAgencyMapper;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyEntity;
import net.chenlin.dp.modules.drtrade.manager.DrtradeAgencyManager;

/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
@Component("drtradeAgencyManager")
public class DrtradeAgencyManagerImpl implements DrtradeAgencyManager {

	@Autowired
	private DrtradeAgencyMapper drtradeAgencyMapper;
	

	@Override
	public List<DrtradeAgencyEntity> listDrtradeAgency(Page<DrtradeAgencyEntity> page, Query search) {
		return drtradeAgencyMapper.listForPage(page, search);
	}

	@Override
	public List<DrtradeAgencyEntity> listDrtradeAgency(Query search) {
		return drtradeAgencyMapper.list(search);
	}

	@Override
	public int saveDrtradeAgency(DrtradeAgencyEntity drtradeAgency) {
		return drtradeAgencyMapper.save(drtradeAgency);
	}

	@Override
	public DrtradeAgencyEntity getDrtradeAgencyById(Long id) {
		DrtradeAgencyEntity drtradeAgency = drtradeAgencyMapper.getObjectById(id);
		return drtradeAgency;
	}

	@Override
	public int updateDrtradeAgency(DrtradeAgencyEntity drtradeAgency) {
		return drtradeAgencyMapper.update(drtradeAgency);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = drtradeAgencyMapper.batchRemove(id);
		return count;
	}

	@Override
	public List<DrtradeAgencyEntity> listNotAgency() {
		return drtradeAgencyMapper.listNotAgency();
	}

	@Override
	public boolean hasChildren(Long[] id) {
		for(Long typeId : id) {
			int count = drtradeAgencyMapper.countAgencyChildren(typeId);
			if(CommonUtils.isIntThanZero(count)) {
				return true;
			}
		}
		return false;
	}
}
