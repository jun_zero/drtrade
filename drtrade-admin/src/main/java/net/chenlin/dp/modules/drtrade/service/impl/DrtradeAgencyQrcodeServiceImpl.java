package net.chenlin.dp.modules.drtrade.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.common.utils.CommonUtils;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyQrcodeEntity;
import net.chenlin.dp.modules.drtrade.manager.DrtradeAgencyQrcodeManager;
import net.chenlin.dp.modules.drtrade.service.DrtradeAgencyQrcodeService;

/**
 * 代理商二维码
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月31日 上午8:47:17
 */
@Service("drtradeAgencyQrcodeService")
public class DrtradeAgencyQrcodeServiceImpl implements DrtradeAgencyQrcodeService {

	@Autowired
	private DrtradeAgencyQrcodeManager drtradeAgencyQrcodeManager;

	@Override
	public Page<DrtradeAgencyQrcodeEntity> listDrtradeAgencyQrcode(Map<String, Object> params) {
		Query query = new Query(params);
		Page<DrtradeAgencyQrcodeEntity> page = new Page<>(query);
		drtradeAgencyQrcodeManager.listDrtradeAgencyQrcode(page, query);
		return page;
	}

	@Override
	public R saveDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity role) {
		int count = drtradeAgencyQrcodeManager.saveDrtradeAgencyQrcode(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getDrtradeAgencyQrcodeById(Long id) {
		DrtradeAgencyQrcodeEntity drtradeAgencyQrcode = drtradeAgencyQrcodeManager.getDrtradeAgencyQrcodeById(id);
		return CommonUtils.msg(drtradeAgencyQrcode);
	}

	@Override
	public R updateDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode) {
		int count = drtradeAgencyQrcodeManager.updateDrtradeAgencyQrcode(drtradeAgencyQrcode);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		int count = drtradeAgencyQrcodeManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	@Override
	public R removeByAgencyId(Integer agencyId) {
		int count = drtradeAgencyQrcodeManager.removeByAgencyId(agencyId);
		return CommonUtils.msg(count);
	}
}
