package net.chenlin.dp.modules.drtrade.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import net.chenlin.dp.modules.base.entity.SysMacroEntity;
import net.chenlin.dp.modules.base.service.SysMacroService;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyQrcodeEntity;
import net.chenlin.dp.modules.drtrade.service.DrtradeAgencyQrcodeService;
import org.iherus.codegen.qrcode.SimpleQrcodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.chenlin.dp.common.annotation.SysLog;
import net.chenlin.dp.modules.sys.controller.AbstractController;
import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyEntity;
import net.chenlin.dp.modules.drtrade.service.DrtradeAgencyService;

/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
@RestController
@RequestMapping("/drtrade/agency")
public class DrtradeAgencyController extends AbstractController {
	
	@Autowired
	private DrtradeAgencyService drtradeAgencyService;

	@Autowired
	private DrtradeAgencyQrcodeService drtradeAgencyQrcodeService;

	@Autowired
	private SysMacroService sysMacroService;

	@Value("${file.savePath}")
	private String savePath;

	@Value("${file.qrcodeBaseUrl}")
	private String qrcodeBaseUrl;
	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/pageList")
	public Page<DrtradeAgencyEntity> listPage(@RequestBody Map<String, Object> params) {
		return drtradeAgencyService.listDrtradeAgency(params);
	}

	/**
	 * 列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public List<DrtradeAgencyEntity> list(@RequestBody Map<String, Object> params) {
		return drtradeAgencyService.listDrtradeAgencyNotPage(params);
	}

	/**
	 * 树形列表
	 * @return
	 */
	@RequestMapping("/select")
	public List<DrtradeAgencyEntity> select() {
		return drtradeAgencyService.listNotAgency();
	}
	/**
	 * 新增
	 * @param drtradeAgency
	 * @return
	 */
	@SysLog("新增代理商")
	@RequestMapping("/save")
	public R save(@RequestBody DrtradeAgencyEntity drtradeAgency) {
		drtradeAgency.setCreateUserId(getUserId());
		return drtradeAgencyService.saveDrtradeAgency(drtradeAgency);
	}
	
	/**
	 * 根据id查询详情
	 * @param id
	 * @return
	 */
	@RequestMapping("/info")
	public R getById(@RequestBody Long id) {
		return drtradeAgencyService.getDrtradeAgencyById(id);
	}
	
	/**
	 * 修改
	 * @param drtradeAgency
	 * @return
	 */
	@SysLog("修改代理商")
	@RequestMapping("/update")
	public R update(@RequestBody DrtradeAgencyEntity drtradeAgency) {
		drtradeAgency.setModifyUserId(getUserId());
		return drtradeAgencyService.updateDrtradeAgency(drtradeAgency);
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@SysLog("删除代理商")
	@RequestMapping("/remove")
	public R batchRemove(@RequestBody Long[] id) {
		return drtradeAgencyService.batchRemove(id);
	}

	@SysLog("生成二维码")
	@RequestMapping("/genQrcode")
	public R genQrcode(@RequestBody DrtradeAgencyEntity drtradeAgency){
		List<SysMacroEntity> macroList = sysMacroService.findByTypeName("规格型号");
		SimpleQrcodeGenerator qrcodeGenerator = new SimpleQrcodeGenerator();
		drtradeAgencyQrcodeService.removeByAgencyId(drtradeAgency.getAgencyId());
		for (SysMacroEntity macro : macroList) {
			String qrcodePath = "qrcode/"+drtradeAgency.getAgencyId()+"/"+macro.getName()+".png";
			File file = new File(savePath+"qrcode/"+drtradeAgency.getAgencyId());
			if(!file.exists()){
				file.mkdir();
			}
			try {
				qrcodeGenerator.generate(qrcodeBaseUrl+macro.getMacroId()
						+"/"+drtradeAgency.getAgencyId()).toFile(savePath+qrcodePath);
				DrtradeAgencyQrcodeEntity qrcodeEntity = new DrtradeAgencyQrcodeEntity();
				qrcodeEntity.setAgencyId(drtradeAgency.getAgencyId());
				qrcodeEntity.setQrcodeName(macro.getName());
				qrcodeEntity.setQrcodePath(qrcodePath);
				drtradeAgencyQrcodeService.saveDrtradeAgencyQrcode(qrcodeEntity);
			} catch (IOException e){
				return R.error(500,"生成二维码失败");
			}
		}
		return R.ok("生成二维码成功");
	}
}
