package net.chenlin.dp.modules.sys.service;

import java.util.List;

import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.modules.sys.entity.SysOrgEntity;

/**
 * 组织机构
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2017年8月17日 上午11:32:55
 */
public interface SysOrgService {

	List<SysOrgEntity> listOrg();
	
	List<SysOrgEntity> listOrgTree();
	
	R saveOrg(SysOrgEntity org);
	
	R getOrg(Long orgId);
	
	R updateOrg(SysOrgEntity org);
	
	R bactchRemoveOrg(Long[] id);
	
}
