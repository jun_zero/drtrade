package net.chenlin.dp.modules.drtrade.service;

import java.util.List;
import java.util.Map;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyEntity;

/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
public interface DrtradeAgencyService {

	Page<DrtradeAgencyEntity> listDrtradeAgency(Map<String, Object> params);

	List<DrtradeAgencyEntity> listDrtradeAgencyNotPage(Map<String, Object> params);

	R saveDrtradeAgency(DrtradeAgencyEntity drtradeAgency);
	
	R getDrtradeAgencyById(Long id);
	
	R updateDrtradeAgency(DrtradeAgencyEntity drtradeAgency);
	
	R batchRemove(Long[] id);

	List<DrtradeAgencyEntity> listNotAgency();
}
