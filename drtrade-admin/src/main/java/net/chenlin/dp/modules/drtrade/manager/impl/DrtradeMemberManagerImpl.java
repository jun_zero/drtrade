package net.chenlin.dp.modules.drtrade.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.modules.drtrade.dao.DrtradeMemberMapper;
import net.chenlin.dp.modules.drtrade.entity.DrtradeMemberEntity;
import net.chenlin.dp.modules.drtrade.manager.DrtradeMemberManager;

/**
 * 
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月27日 下午4:45:46
 */
@Component("drtradeMemberManager")
public class DrtradeMemberManagerImpl implements DrtradeMemberManager {

	@Autowired
	private DrtradeMemberMapper drtradeMemberMapper;
	

	@Override
	public List<DrtradeMemberEntity> listDrtradeMember(Page<DrtradeMemberEntity> page, Query search) {
		return drtradeMemberMapper.listForPage(page, search);
	}

	@Override
	public int saveDrtradeMember(DrtradeMemberEntity drtradeMember) {
		return drtradeMemberMapper.save(drtradeMember);
	}

	@Override
	public DrtradeMemberEntity getDrtradeMemberById(Long id) {
		DrtradeMemberEntity drtradeMember = drtradeMemberMapper.getObjectById(id);
		return drtradeMember;
	}

	@Override
	public int updateDrtradeMember(DrtradeMemberEntity drtradeMember) {
		return drtradeMemberMapper.update(drtradeMember);
	}

	@Override
	public int batchRemove(Long[] id) {
		int count = drtradeMemberMapper.batchRemove(id);
		return count;
	}

	@Override
	public DrtradeMemberEntity getByOpenid(String openid) {
		DrtradeMemberEntity drtradeMember = drtradeMemberMapper.getByOpenid(openid);
		return drtradeMember;
	}
}
