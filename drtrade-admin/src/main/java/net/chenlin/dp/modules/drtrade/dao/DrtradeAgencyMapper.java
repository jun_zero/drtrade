package net.chenlin.dp.modules.drtrade.dao;

import org.apache.ibatis.annotations.Mapper;

import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyEntity;
import net.chenlin.dp.modules.sys.dao.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
@Mapper
public interface DrtradeAgencyMapper extends BaseMapper<DrtradeAgencyEntity> {

    List<DrtradeAgencyEntity> listNotAgency();

    int countAgencyChildren(Long typeId);
}
