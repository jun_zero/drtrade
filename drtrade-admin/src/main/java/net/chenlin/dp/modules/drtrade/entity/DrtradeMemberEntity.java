package net.chenlin.dp.modules.drtrade.entity;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月27日 下午4:45:46
 */
public class DrtradeMemberEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	private Long memberId;
	
	/**
	 * 
	 */
	private String openid;
	
	/**
	 * 
	 */
	private String sex;
	
	/**
	 * 
	 */
	private String nickname;
	
	/**
	 * 
	 */
	private String province;
	
	/**
	 * 
	 */
	private String city;
	
	/**
	 * 
	 */
	private String country;
	
	/**
	 * 
	 */
	private String headimgurl;
	
	/**
	 * 
	 */
	private String unionid;
	

	public DrtradeMemberEntity() {
		super();
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	
	public Long getMemberId() {
		return memberId;
	}
	
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	public String getOpenid() {
		return openid;
	}
	
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getSex() {
		return sex;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}
	
	public String getHeadimgurl() {
		return headimgurl;
	}
	
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	
	public String getUnionid() {
		return unionid;
	}
	
}
