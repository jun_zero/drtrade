package net.chenlin.dp.modules.drtrade.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
public class DrtradeAgencyEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 代理商id
	 */
	private Integer agencyId;
	
	/**
	 * 代理商名称
	 */
	private String agencyName;
	
	/**
	 * 联系人
	 */
	private String linkman;
	
	/**
	 * 联系电话
	 */
	private String linktel;
	
	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 上级代理商id
	 */
	private Integer parentId;
	
	/**
	 * 创建用户id
	 */
	private Long createUserId;
	
	/**
	 * 修改用户id
	 */
	private Long modifyUserId;
	
	/**
	 * 修改时间
	 */
	private Date gmtModify;
	
	/**
	 * 创建时间
	 */
	private Date gmtCreate;

	private List<DrtradeAgencyQrcodeEntity> qrcodeList;

	/**
	 * ztree属性
	 */
	private Boolean open;

	private String parentName;

	public DrtradeAgencyEntity() {
		super();
	}

	public void setAgencyId(Integer agencyId) {
		this.agencyId = agencyId;
	}
	
	public Integer getAgencyId() {
		return agencyId;
	}
	
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	
	public String getAgencyName() {
		return agencyName;
	}
	
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	
	public String getLinkman() {
		return linkman;
	}
	
	public void setLinktel(String linktel) {
		this.linktel = linktel;
	}
	
	public String getLinktel() {
		return linktel;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	public Integer getParentId() {
		return parentId;
	}
	
	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}
	
	public Long getCreateUserId() {
		return createUserId;
	}
	
	public void setModifyUserId(Long modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	
	public Long getModifyUserId() {
		return modifyUserId;
	}
	
	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}
	
	public Date getGmtModify() {
		return gmtModify;
	}
	
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	public Date getGmtCreate() {
		return gmtCreate;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<DrtradeAgencyQrcodeEntity> getQrcodeList() {
		return qrcodeList;
	}

	public void setQrcodeList(List<DrtradeAgencyQrcodeEntity> qrcodeList) {
		this.qrcodeList = qrcodeList;
	}
}
