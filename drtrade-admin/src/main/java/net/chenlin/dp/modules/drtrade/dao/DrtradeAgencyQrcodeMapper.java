package net.chenlin.dp.modules.drtrade.dao;

import org.apache.ibatis.annotations.Mapper;

import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyQrcodeEntity;
import net.chenlin.dp.modules.sys.dao.BaseMapper;

/**
 * 代理商二维码
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月31日 上午8:47:17
 */
@Mapper
public interface DrtradeAgencyQrcodeMapper extends BaseMapper<DrtradeAgencyQrcodeEntity> {
    int removeByAgencyId(Integer agencyId);
}
