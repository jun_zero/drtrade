package net.chenlin.dp.modules.drtrade.manager;

import java.util.List;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyEntity;

/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
public interface DrtradeAgencyManager {

	List<DrtradeAgencyEntity> listDrtradeAgency(Page<DrtradeAgencyEntity> page, Query search);

	List<DrtradeAgencyEntity> listDrtradeAgency(Query search);

	int saveDrtradeAgency(DrtradeAgencyEntity drtradeAgency);
	
	DrtradeAgencyEntity getDrtradeAgencyById(Long id);
	
	int updateDrtradeAgency(DrtradeAgencyEntity drtradeAgency);
	
	int batchRemove(Long[] id);

	List<DrtradeAgencyEntity> listNotAgency();

	boolean hasChildren(Long[] id);
}
