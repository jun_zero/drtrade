package net.chenlin.dp.modules.drtrade.manager;

import java.util.List;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyQrcodeEntity;

/**
 * 代理商二维码
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月31日 上午8:47:17
 */
public interface DrtradeAgencyQrcodeManager {

	List<DrtradeAgencyQrcodeEntity> listDrtradeAgencyQrcode(Page<DrtradeAgencyQrcodeEntity> page, Query search);
	
	int saveDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode);
	
	DrtradeAgencyQrcodeEntity getDrtradeAgencyQrcodeById(Long id);
	
	int updateDrtradeAgencyQrcode(DrtradeAgencyQrcodeEntity drtradeAgencyQrcode);

	int batchRemove(Long[] id);

	int removeByAgencyId(Integer agencyId);
}
