package net.chenlin.dp.modules.drtrade.service.impl;

import java.util.List;
import java.util.Map;

import net.chenlin.dp.common.constant.MsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.chenlin.dp.common.entity.Page;
import net.chenlin.dp.common.entity.Query;
import net.chenlin.dp.common.entity.R;
import net.chenlin.dp.common.utils.CommonUtils;
import net.chenlin.dp.modules.drtrade.entity.DrtradeAgencyEntity;
import net.chenlin.dp.modules.drtrade.manager.DrtradeAgencyManager;
import net.chenlin.dp.modules.drtrade.service.DrtradeAgencyService;

/**
 * 代理商
 *
 * @author drtrade
 * @email admin@drtrade.cn
 * @url www.drtrade.cn
 * @date 2018年3月18日 上午10:59:20
 */
@Service("drtradeAgencyService")
public class DrtradeAgencyServiceImpl implements DrtradeAgencyService {

	@Autowired
	private DrtradeAgencyManager drtradeAgencyManager;

	@Override
	public Page<DrtradeAgencyEntity> listDrtradeAgency(Map<String, Object> params) {
		Query query = new Query(params);
		Page<DrtradeAgencyEntity> page = new Page<>(query);
		drtradeAgencyManager.listDrtradeAgency(page, query);
		return page;
	}

	@Override
	public List<DrtradeAgencyEntity> listDrtradeAgencyNotPage(Map<String, Object> params) {
		Query query = new Query(params);
		return drtradeAgencyManager.listDrtradeAgency(query);
	}

	@Override
	public R saveDrtradeAgency(DrtradeAgencyEntity role) {
		int count = drtradeAgencyManager.saveDrtradeAgency(role);
		return CommonUtils.msg(count);
	}

	@Override
	public R getDrtradeAgencyById(Long id) {
		DrtradeAgencyEntity drtradeAgency = drtradeAgencyManager.getDrtradeAgencyById(id);
		return CommonUtils.msg(drtradeAgency);
	}

	@Override
	public R updateDrtradeAgency(DrtradeAgencyEntity drtradeAgency) {
		int count = drtradeAgencyManager.updateDrtradeAgency(drtradeAgency);
		return CommonUtils.msg(count);
	}

	@Override
	public R batchRemove(Long[] id) {
		boolean children = drtradeAgencyManager.hasChildren(id);
		if(children) {
			return R.error(MsgConstant.MSG_HAS_CHILD);
		}
		int count = drtradeAgencyManager.batchRemove(id);
		return CommonUtils.msg(id, count);
	}

	@Override
	public List<DrtradeAgencyEntity> listNotAgency() {
		List<DrtradeAgencyEntity> agencys = drtradeAgencyManager.listNotAgency();
		DrtradeAgencyEntity agency = new DrtradeAgencyEntity();
		agency.setAgencyId(0);
		agency.setParentId(-1);
		agency.setAgencyName("总代理");
		agency.setOpen(true);
		agencys.add(agency);
		return agencys;
	}
}
